(function(window) {
    'use strict';

    var document = window.document,
        auth = document.querySelector('#auth'),
        logout = document.querySelector('#logout'),
        controls = document.querySelector('#controls'),
        menu = controls.querySelector('.toggle');

    function GPSTracker() {
        this.options = {};

        if (window.GPSTrackerOptions) {
            Object.assign(this.options, window.GPSTrackerOptions);
        }

        this.status().then(response => {
            if (response.authed) {
                this.onLoggedIn();
            }
        });

        auth.addEventListener('submit', e => {
            e.preventDefault();

            this.auth(new FormData(e.target), response => {
                if (response.status === 200) {
                    e.target.reset();
                    this.onLoggedIn();
                }
            });
        });

        logout.addEventListener('click', e => {
            e.preventDefault();

            this.request('post', 'logout').then(response => {
                if (response.status === 200) {
                    this.onLoggedOut();
                }
            });
        });

        window.addEventListener('mousedown', e => {
            controls.classList.remove('open');
        });

        controls.addEventListener('mousedown', e => {
            e.stopPropagation();
        });

        controls.addEventListener('submit', e => {
            e.preventDefault();

            this.getPositions(new FormData(e.target), response => {
                console.log('callback');
                //this.drawClusteredMarkers(response.rows);
                this.drawPolyline(response.rows);
            }).then(response => {
                console.log('alles done')
            });
        });

        menu.addEventListener('click', e => {
            controls.classList.toggle('open');
        });

        this.initMap();
    }

    GPSTracker.prototype.status = function() {
        return this.request('GET', 'status').then(response => {
            return response.json();
        });
    };

    GPSTracker.prototype.auth = function(body, callback) {
        return this.request('POST', 'auth', body).then(response => {
            if (typeof callback === 'function') {
                callback.call(this, response);
            }

            return response.json();
        }).then(json => {
            new Notif(json.message);
        });
    };

    GPSTracker.prototype.getTracks = function() {
        return this.request('GET', 'get-tracks').then(response => {
            return response.json();
        });
    };

    GPSTracker.prototype.getPositions = function(options, callback) {
        return this.request('GET', 'get-positions', options).then(response => {
            return response.json();
        }).then(json => {
            return new Promise((resolve, reject) => {
                var page = options.has('page')
                    ? parseInt(options.get('page'))
                    : 1;

                callback(json);

                if (json.pages <= 1 || json.pages === page) {
                    return resolve(json);
                }

                options.set('page', page + 1);
                return resolve(this.getPositions(options, callback));
            });
        });
    };

    GPSTracker.prototype.request = function(method, action, data, _options) {
        var method = method.toUpperCase();
        var url = this.options.apiUrl;
        var options = {
            method: method,
            credentials: 'include',
        };

        if (!data) {
            data = new URLSearchParams();
        }

        data.set('action', action);

        if (method === 'GET' || method === 'HEAD') {
            url += '?' + new URLSearchParams(data).toString();
        } else {
            options['body'] = data;
        }

        if (_options) {
            Object.assign(options, _options);
        }

        return fetch(url, options);
    };

    GPSTracker.prototype.onLoggedIn = function() {
        auth.style.display = 'none';
        controls.style.display = 'block';

        let now = luxon.DateTime.utc();
        let yesterday = now.minus({days: 1});
        let format = 'yyyy-MM-dd HH:mm:ss';
        controls.querySelector('[name="start"]').value = yesterday.toFormat(format);
        controls.querySelector('[name="end"]').value = now.toFormat(format);

        this.getTracks().then(response => {
            var select = controls.querySelector('[name="track_id"]');

            response.rows.forEach(row => {
                var opt = document.createElement('option');
                opt.value = row.id;
                opt.innerText = `${row.name} (${row.added})`;

                select.append(opt);
            });
        });
    };

    GPSTracker.prototype.onLoggedOut = function() {
        auth.style.removeProperty('display');
        controls.style.removeProperty('display');
    };

    GPSTracker.prototype.initMap = function() {
        this.baseLayer = L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png', {
            attribution: 'Map tiles by Carto, under CC BY 3.0. Data by OpenStreetMap, under ODbL.',
            maxZoom: 18,
        });

        this.markerLayer = L.markerClusterGroup();

        this.heatmapLayer = new HeatmapOverlay({
            radius: 6,
            latField: 'latitude',
            lngField: 'longitude',
        });

        this.icon = L.icon({
            iconUrl: 'assets/img/marker.png',
        });

        this.map = L.map('map', {
            center: new L.LatLng(61, 25),
            zoom: 4,
            layers: [this.baseLayer, this.markerLayer, this.heatmapLayer],
        });

        //this.getData().then(data => {
            //this.heatmap.setData({
                //max: 8,
                //data: data
            //});
        //});
    };

    GPSTracker.prototype.drawClusteredMarkers = function(data) {
        data.forEach(row => {
            this.markerLayer.addLayer(
                L.marker(
                    [row.latitude, row.longitude],
                    {
                        icon: this.icon,
                    }
                )
            );
        });
    };

    GPSTracker.prototype.drawPolyline = function(data) {
        var latlngs = [];

        data.forEach(row => {
            latlngs.push([row.latitude, row.longitude]);
        });

        L.polyline(latlngs, {
            color: '#000',
            opacity: 0.6,
        }).addTo(this.map);
    };

    GPSTracker.prototype.getPage = function(page, limit) {
        var params = new URLSearchParams({
            action: 'get-positions',
        });

        page && params.append('page', page);
        limit && params.append('limit', limit);

        var request = new Request('client/?' + params.toString());

        return fetch(request).then(response => {
            return response.json();
        });
    };

    GPSTracker.prototype.getData = function() {
        return new Promise((resolve, reject) => {
            this.getPage().then(json => {
                var data = json.rows;

                if (json.pages <= 1) {
                    resolve(data);
                }

                var promises = [];
                for (var i = 2; i <= json.pages; i++) {
                    promises.push(this.getPage(i).then(json => {
                        json.rows.forEach(v => {
                            data.push(v);
                        })
                    }));
                }

                Promise.all(promises).then(() => {
                    resolve(data);
                });
            });
        });
    };

    window.GPSTracker = new GPSTracker();
})(this);
