(function(window) {
    'use strict';

    var document = window.document;

    var area = document.querySelector('#notifications');

    function Notif(text) {
        var n = document.createElement('div');

        n.innerText = text;

        n.onclick = function() {
            n.remove();
        };

        setTimeout(() => {
            n.remove();
        }, 3000);

        area.append(n);
    }

    window.Notif = Notif;
})(this);
